#### 

#### **一、菜品详情**

| 字段名         | 字段类型 | 是否可空 | 释义     |
| -------------- | -------- | -------- | -------- |
| CId            | int      | false    | 主键     |
| CName          | string   | false    | 菜品名称 |
| CImg           | string   | false    | 菜品图片 |
| CNum           | int      | false    | 评论数量 |
| CWriter        | string   | false    | 作者     |
| TypeId         | int      | false    | 菜系ID   |
| TypeCode       | int      | false    | 分类ID   |
| ScreenId       | int      | false    | 筛选ID   |
| CLngredients   | string   | false    | 主料     |
| CSupplementary | string   | false    | 辅料     |
| CStep          | string   | false    | 步骤     |
| CCooking       | string   | false    | 烹饪技巧 |
| UId            | int      | false    | 用户ID   |
| Status         | int      | false    | 状态     |

#### 二、菜系

| 字段名   | 字段类型 | 是否可空 | 释义     |
| -------- | -------- | -------- | -------- |
| TypeId   | int      | false    | 主键     |
| TypeName | string   | false    | 菜系名称 |
| TypeCode | int      | false    | 二级联动 |

#### 三、筛选

| 字段名     | 字段类型 | 是否可空 | 释义     |
| ---------- | -------- | -------- | -------- |
| ScreenId   | int      | false    | 主键     |
| ScreenName | string   | false    | 筛选名称 |

#### 

#### 四、用户表

| 字段名    | 字段类型 | 是否可空 | 释义     |
| --------- | -------- | -------- | -------- |
| UId       | int      | false    | 用户ID   |
| UName     | string   | false    | 用户名   |
| Password  | string   | false    | 密码     |
| UImg      | string   | false    | 用户图片 |
| USynopsis | string   | false    | 用户简介 |
| UPing     | string   | false    | 用户评价 |

