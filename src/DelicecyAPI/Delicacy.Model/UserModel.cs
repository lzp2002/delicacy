﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Model
{
    [Table("Users")]
    public class UserModel
    {
        [Key]
        public int UId { get; set; }
        public string UName { get; set; }
        public string Password { get; set; }
        public string UImg { get; set; }
        public string USynopsis { get; set; }
        public string UPing { get; set; }
    }
}
