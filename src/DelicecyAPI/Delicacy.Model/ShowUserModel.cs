﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Model
{
    public class ShowUserModel
    {
        public int UId { get; set; }
        public string UName { get; set; }
        public string Password { get; set; }
        public string UImg { get; set; }
        public string USynopsis { get; set; }
        public string UPing { get; set; }
    }
}
