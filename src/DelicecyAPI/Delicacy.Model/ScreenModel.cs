﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Model
{
    [Table("Screen")]
    public class ScreenModel
    {
        [Key]
        public int ScreenId { get; set; }
        public string ScreenName { get; set; }
    }
}
