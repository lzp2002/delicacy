﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Model
{
    [Table("Food")]
    public class FoodModel
    {
        [Key]
        public int CId { get; set; }
        public string CName { get; set; }
        public string CImg { get; set; }
        public int CNum { get; set; }
        public string CWriter { get; set; }
        public int TypeId { get; set; }
        public int TypeCode { get; set; }
        public int ScreenID { get; set; }
        public string Title { get; set; }
        public string CLngredients { get; set; }
        public string CSupplementary { get; set; }
        public string Step { get; set; }
        public string CCokking { get; set; }
        public int Status { get; set; } = 0;
    }
}
