﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Model
{
    [Table("CookiType")]
    public class CookiType
    {
        [Key]
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int TypeCode { get; set; }
    }
}
