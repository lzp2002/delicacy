﻿using Delicacy.Bll;
using Delicacy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;

namespace DelicecyAPI.Controllers
{
    public class DelicacyController : ApiController
    {
        LoginBll loginBll = new LoginBll();
        DelicacyBll delicacyBll = new DelicacyBll();
        [HttpPost,Route("api/Login")]
        public IHttpActionResult Login(string uname, string password)
        {
            var result = loginBll.Login(uname, password);
            if(result!=null)
            {
                return Json(new { code = 1, msg = "登录成功", data = result });
            }
            else
            {
                return Json(new { code = 0, msg = "登录失败", data = "" });
            }
        }
        [HttpPost, Route("api/Regiter")]
        public IHttpActionResult Regiter(UserModel userModel)
        {
            return Json(loginBll.Regiter(userModel));
        }
        [HttpGet,Route("api/ShowFood")]
        public IHttpActionResult ShowFood()
        {
            return Json(delicacyBll.ShowFood());
        }
        [HttpGet, Route("api/FanFood")]
        public IHttpActionResult FanFood(int id)
        {
            return Json(delicacyBll.FanFood(id));
        }
        [HttpGet, Route("api/ShowPage")]
        public IHttpActionResult ShowPage(int? tid, int? codeid, int? sid,int pageindex=1, int pagesize=2)
        {
            var totalcount = 0;
            var list= delicacyBll.ShowPage(pageindex, pagesize, tid, codeid, sid, out totalcount);
            return Json(new { list, totalcount });
        }
        [HttpGet, Route("api/BindType")]
        public IHttpActionResult BindType(int id)
        {
            return Json(delicacyBll.BindType(id));
        }
        [HttpGet, Route("api/BindScreen")]
        public IHttpActionResult BindScreen()
        {
            return Json(delicacyBll.BindScreen());
        }
        [HttpPost, Route("api/AddFood")]
        public IHttpActionResult AddFood(FoodModel foodModel)
        {
            return Json(delicacyBll.AddFood(foodModel));
        }
        [HttpPost, Route("api/UpLoad")]
        public IHttpActionResult UpLoad()
        {
            var tu = HttpContext.Current.Request.Files[0];
            var path = HttpContext.Current.Server.MapPath("/Images/");
            tu.SaveAs(path + tu.FileName);
            return Json(new { Url = tu.FileName });
        }
        [HttpGet,Route("api/ShowUser")]
        public IHttpActionResult ShowUser(int id)
        {
            return Json(delicacyBll.ShowUser(id));
        }
        [HttpGet, Route("api/Fan")]
        public IHttpActionResult Fan(int id)
        {
            return Json(delicacyBll.Fan(id));
        }
        [HttpPost, Route("api/Change")]
        public IHttpActionResult Change(UserModel userModel)
        {
            return Json(delicacyBll.Change(userModel));
        }
    }
}