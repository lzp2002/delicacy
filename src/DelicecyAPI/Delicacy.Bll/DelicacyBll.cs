﻿using Delicacy.Dal;
using Delicacy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Bll
{
    public class DelicacyBll
    {
        DelicacyDal delicacyDal = new DelicacyDal();
        public List<ShowFoodFirstModel> ShowFood()
        {
            return delicacyDal.ShowFood();
        }
        public FoodModel FanFood(int id)
        {
            return delicacyDal.FanFood(id);
        }
        public List<ShowModel> ShowPage(int pageindex, int pagesize, int? tid, int? codeid, int? sid, out int totalcount)
        {
            return delicacyDal.ShowPage(pageindex, pagesize, tid, codeid, sid, out totalcount);
        }
        public List<CookiType> BindType(int id)
        {
            return delicacyDal.BindType(id);
        }
        public List<ScreenModel> BindScreen()
        {
            return delicacyDal.BindScreen();
        }
        public int AddFood(FoodModel foodModel)
        {
            return delicacyDal.AddFood(foodModel);
        }
        public List<ShowUserModel> ShowUser(int id)
        {
            return delicacyDal.ShowUser(id);
        }
        public UserModel Fan(int id)
        {
            return delicacyDal.Fan(id);
        }
        public int Change(UserModel userModel)
        {
            return delicacyDal.Change(userModel);
        }
    }
}
