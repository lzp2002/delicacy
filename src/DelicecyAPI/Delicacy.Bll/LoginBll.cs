﻿using Delicacy.Dal;
using Delicacy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Bll
{
    public class LoginBll
    {
        LoginDal loginDal = new LoginDal();
        public UserModel Login(string uname, string password)
        {
            return loginDal.Login(uname, password);
        }
        public int Regiter(UserModel userModel)
        {
            return loginDal.Regiter(userModel);
        }
    }
}
