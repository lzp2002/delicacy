﻿using Delicacy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Dal
{
    public class LoginDal
    {
        DelicacyDbContext db = new DelicacyDbContext();
        public UserModel Login(string uname, string password)
        {
            return db.Users.FirstOrDefault(m => m.UName == uname && m.Password == password);
        }
        public int Regiter(UserModel userModel)
        {
            db.Users.Add(userModel);
            return db.SaveChanges();
        }
    }
}
