namespace Delicacy.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CookiType",
                c => new
                    {
                        TypeId = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                        TypeCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TypeId);
            
            CreateTable(
                "dbo.Food",
                c => new
                    {
                        CId = c.Int(nullable: false, identity: true),
                        CName = c.String(),
                        CImg = c.String(),
                        CNum = c.Int(nullable: false),
                        CWriter = c.String(),
                        TypeId = c.Int(nullable: false),
                        TypeCode = c.Int(nullable: false),
                        ScreenID = c.Int(nullable: false),
                        Title = c.String(),
                        CLngredients = c.String(),
                        CSupplementary = c.String(),
                        Step = c.String(),
                        CCokking = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CId);
            
            CreateTable(
                "dbo.Screen",
                c => new
                    {
                        ScreenId = c.Int(nullable: false, identity: true),
                        ScreenName = c.String(),
                    })
                .PrimaryKey(t => t.ScreenId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UId = c.Int(nullable: false, identity: true),
                        UName = c.String(),
                        Password = c.String(),
                        UImg = c.String(),
                        USynopsis = c.String(),
                        UPing = c.String(),
                    })
                .PrimaryKey(t => t.UId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
            DropTable("dbo.Screen");
            DropTable("dbo.Food");
            DropTable("dbo.CookiType");
        }
    }
}
