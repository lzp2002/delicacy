﻿using Delicacy.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delicacy.Dal
{
    public class DelicacyDal
    {
        DelicacyDbContext db = new DelicacyDbContext();
        public List<ShowFoodFirstModel> ShowFood()
        {
            var query = from a in db.Foods
                        select new ShowFoodFirstModel
                        {
                            CId = a.CId,
                            CName = a.CName,
                            CImg = "http://localhost:55943/Images/" + a.CImg,
                            CNum = a.CNum,
                            CWriter = a.CWriter
                        };
            return query.ToList();
        }
        public FoodModel FanFood(int id)
        {
            return db.Foods.FirstOrDefault(m => m.CId == id);
        }
        public List<ShowModel> ShowPage(int pageindex,int pagesize,int? tid,int? codeid,int? sid,out int totalcount)
        {
            var query = from a in db.Foods
                        join b in db.Screens on a.ScreenID equals b.ScreenId
                        join c in db.CookiTypes on a.TypeId equals c.TypeId
                        join d in db.CookiTypes on a.TypeCode equals d.TypeId
                        select new ShowModel
                        {
                            CId = a.CId,
                            CName = a.CName,
                            CImg = "http://localhost:55943/Images/" + a.CImg,
                            CNum = a.CNum,
                            CWriter = a.CWriter,
                            TypeId=a.TypeId,
                            TName=c.TypeName,
                            TypeCode=a.TypeCode,
                            TCName=d.TypeName,
                            ScreenID=a.ScreenID,
                            SName=b.ScreenName
                        };
            if(tid>0)
            {
                query = query.Where(m => m.TypeId == tid);
            }
            if (codeid > 0)
            {
                query = query.Where(m => m.TypeCode == codeid);
            }
            if (sid>0)
            {
                query = query.Where(m => m.ScreenID == sid);
            }
            totalcount = query.Count();
            return query.OrderBy(m => m.CId).Skip((pageindex - 1) * pagesize).Take(pagesize).ToList();
        }
        public List<CookiType> BindType(int id)
        {
            return db.CookiTypes.Where(m => m.TypeCode == id).ToList();
        }
        public List<ScreenModel> BindScreen()
        {
            return db.Screens.ToList();
        }
        public int AddFood(FoodModel foodModel)
        {
            db.Foods.Add(foodModel);
            return db.SaveChanges();
        }
        public List<ShowUserModel> ShowUser(int id)
        {
            var query = from a in db.Users where a.UId==id
                        select new ShowUserModel
                        {
                            UId = a.UId,
                            UName = a.UName,
                            Password = a.Password,
                            UImg = "http://localhost:55943/Images/" + a.UImg,
                            USynopsis = a.USynopsis,
                            UPing = a.UPing
                        };
            return query.ToList();
        }
        public UserModel Fan(int id)
        {
            return db.Users.FirstOrDefault(m => m.UId == id);
        }
        public int Change(UserModel userModel)
        {
            db.Entry(userModel).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges();
        }
    }
}
